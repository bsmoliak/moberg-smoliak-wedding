---
layout: page
title: Program
description: What to expect for the evening.
image: pic-program.jpg
---

<h4>Itinerary</h4>
<div class="table-wrapper">
		<table>
			<thead>
				<tr>
					<th>Location</th>
					<th>Event</th>
					<th>Timing</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Gathering + Ceremony</td>
					<td>4:00-5:00pm</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Social Hour</td>
					<td>5:00-6:00pm</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Bonfire</td>
					<td>5:00-8:00pm</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Dinner</td>
					<td>6:15pm</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Dessert + Dancing</td>
					<td>8:00pm</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Last Call</td>
					<td>10:00pm</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Closing Time</td>
					<td>11:00pm</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">See map below</td>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>

<p><span class="image right"><img src="assets/images/pic-venue-map.png" alt="" /></span>
</p>

## Gathering & Ceremony

Gather with the bride and groom's family and friends at the picnic
shelter just up the hill from the interpretive center. Seating is available
for about 2/3rds of our 120 guests. A mix of folding chairs, picnic tables,
and standing room will be available. Other than the front row, no seating is
reserved. We encourage you to sit or stand for  the 20 minute ceremony,
whatever your preference.

## Social Hour

Stroll or wheel down the hill to the interpretive center patio for a social
hour. A variety of non-alcoholic and alcoholic drinks will be available from
two bars. NA beverages, beer, and wine are hosted all evening. *Spirits and cocktails
will be available for purchase.* We've filled the tip jar with 10%, please
consider matching as a courtesy.

### Appetizers

* __Crudité__: English cucumbers, colorful carrots, watermelon radishes, sweet bell peppers, and cruciferous Romanesco and cauliflower alongside Edamame Pâté and Sundried Tomato Dip
* __Mezze__: Mediterranean fare featuring Curried Cauliflower Hummus, skordalia, Roasted Carrot Pesto, lemony artichokes, mixed olives, peppadew peppers, cucumbers, watermelon radish, carrots, and other delights. Served with pita, lavash, and gluten-free crackers.

### NA beverages

* __Soda (aka Pop)__: Coke, Diet Coke, Sprite, and Ginger Ale
* __Water__: Still, Sparkling (Topo Chico)
* __Juice__: Orange, Cranberry, Lemonade, Pineapple
* __Mixers__: Club Soda, Tonic Water, Margarita Mix, Simple Syrup, Grenadine
* __[Heineken 0.0 non-alcoholic beer](https://untappd.com/b/heineken-heineken-0-0/1977303)__

### Beer & Cider

* __[Miller High Life](https://untappd.com/b/miller-brewing-company-high-life/3778)__, the champagne of beers
* __[Utipils Receptional](https://untappd.com/b/utepils-brewing-co-receptional/2293519)__, a festbier fit for a wedding
* __[Bauhaus Lounge Wizard](https://untappd.com/b/bauhaus-brew-labs-lounge-wizard/3146627)__, a hazy pale ale
* __[Sociable Skinny Tire](https://untappd.com/b/sociable-cider-werks-skinny-tire/3779011)__, a light cider

### Wine

* __[Moscato D'Asti, Rivata](https://www.vivino.com/US-MN/en/casa-vinicola-ferrero-rivata-moscato-d-asti/w/1192890)__, a sweet white
* __[Sauvignon Blanc, Kia Ora](https://www.vivino.com/US-MN/en/kia-ora-sauvignon-blanc/w/1822262?ref=nav-search)__, a crisp white
* __[Red Blend, Apothic](https://www.vivino.com/US-MN/en/apothic-red-winemaker-s-blend/w/1130327)__, a smooth red
* __[Cabernet Sauvignon, Clos de Napa Reserve](https://www.vivino.com/US-MN/en/clos-de-napa-reserve-cabernet-sauvignon-napa-valley/w/9325711)__, a silky red

## Bonfire

A Westwood Hills Nature Center staff member will host a bonfire
ring between the picnic shelter and the interpretive center from 5:00pm
to 8:00pm. Step away from the party for a quiet moment, just leave any
alcoholic beverages behind.

## Dinner

Our wedding dinner will be buffet-style and plant-based, fresh from
Reverie Cafe in Minneapolis. Each table setting will have a glass for
water, a glass for wine, silverware, napkin, and compostable plate.

* __Green Salad__, Mesclun Greens served with Cucumber, Red Onion, and Hemp Seeds
* __Market Vegetable__, roasted Broccoli, Red Peppers, and Lemon Garlic Sesame Marinade
* __Korean BBQ Mock Duck__, served with Coconut Rice, Pickled Veggies, Radish, & Cilantro (*contains gluten*)
* __Smoked Gouda Mac & Cheese__, served with Shiitake Mushroom Bacon + Crispy Panko (*contains cashews and gluten*)

Guests with a tree nut or gluten-allergy who cannot have the Mock Duck or Mac & Cheese
will get:

* __Cuban Jackfruit Bowl__, served with Pickled Onions, Coconut Rice, & Cilantro.

## Dessert & Dancing

After dinner the bride & groom will share a cutting cake and
provide guests with a classic dessert snack appealing to your
inner child. Coffee and tea (black, green, and herbal) will
be available as well. Then we'll turn up the music and open
up the dance floor.

Last call for drinks will happen at 10:00pm, and bar service
will end promptly at 10:30pm. The music stops at 11:00pm,
bringing this special event to a close.

## Vendors

* [Venue](https://www.stlouisparkmn.gov/government/departments-divisions/parks-rec/westwood-hills-nature-center)
* [Bartenders](https://twistbartendingservice.com/mn/)
* [Appetizers](https://www.chowgirls.net)
* [Dinner](https://www.reveriempls.com)
* [Dessert](https://www.harkcafe.com)
