---
layout: page
title: Guide Book
description: A collection of information for visitors to the Twin Cities.
image: pic-guide.jpg
---

## Fall Color Update

Leaves are starting to change color in the Twin Cities metro area!

<div class="box alt">
		<div class="row uniform 50%">
			<div class="5u"><span class="image fit"><a href="https://www.dnr.state.mn.us/fall_colors/index.html"><img src="assets/images/pic-fall-color-update.png" alt="" /></a></span></div>
		</div>
</div>

## Local Attractions

### For art-lovers

* __[Minneapolis Sculpture Garden](https://walkerart.org/visit/garden)__, public art.
  * *Cost*: Free
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=726%20Vineland%20Pl,%20Minneapolis,%20MN%20%2055403,%20United%20States&auid=9338986508944331487&ll=44.969345,-93.289300&lsp=9902&q=Minneapolis%20Sculpture%20Garden)
* __[Walker Art Center](https://walkerart.org)__, get your modern art fix.
  * *Cost*: $$
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=725%20Vineland%20Pl,%20Minneapolis,%20MN%20%2055403,%20United%20States&auid=16844384047172265966&ll=44.967746,-93.288462&lsp=9902&q=Walker%20Art%20Center)
* __[Minneapolis Institute of Art](https://new.artsmia.org)__, ancient and contemporary art.
  * *Cost*: Free
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=2400%20Third%20Ave%20S,%20Minneapolis,%20MN%2055404,%20United%20States&auid=4436173179397683368&ll=44.958153,-93.273249&lsp=9902&q=Minneapolis%20Institute%20of%20Art)
* __[Hopkins Center for the Arts](https://www.hopkinsartscenter.com/149/Exhibitions-at-the-Center)__, contemporary arts center nearby the couple's home.
  * *Cost*: Free
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=1111%20Mainstreet,%20Hopkins,%20MN%20%2055343,%20United%20States&auid=11832912813612848285&ll=44.924554,-93.414370&lsp=9902&q=Hopkins%20Center%20for%20the%20Arts)

### For foodies

* __[Revival - SLP](https://www.revivalrestaurants.com/st-louis)__, solid Southern Cusine brunch option for Saturday.
  * *Cost*: $
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=8028%20Minnetonka%20Blvd,%20St.%20Louis%20Park,%20MN%2055426,%20United%20States&auid=14187211471670423756&ll=44.949993,-93.381793&lsp=9902&q=Revival)
* __[Owammi](https://owamni.com)__, a full-service Indigenous restaurant on the Mississippi River (reservations recommended!).
  * *Cost*: $$
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=420%20South%20First%20Street,%20Minneapolis,%20MN%2055401,%20United%20States&auid=7197775256934801356&ll=44.981172,-93.260298&lsp=9902&q=Owamni)
* __[Fika Cafe](https://asimn.org/visit/fika-cafe/)__, Nordic-inspired cafe at the American Swedish Institute.
  * *Cost*: $$
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=2600%20Park%20Ave,%20Minneapolis,%20MN%20%2055407,%20United%20States&auid=3027930540187272403&ll=44.955199,-93.265849&lsp=9902&q=Fika)
* [Alma](https://www.almampls.com), omnivore and vegetarian tasting prix fixe (reservations required).
  * *Cost*: $$$
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=528%20University%20Ave%20SE,%20Minneapolis,%20MN%20%2055414,%20United%20States&auid=14081418109504347699&ll=44.983902,-93.247963&lsp=9902&q=Alma)

### For hipsters

* __[Cream & Amber](https://www.creamandamber.com)__, a delightful indie bookstore near the couple's home.
  * *Cost*: $$
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=1605%20Mainstreet,%20Hopkins,%20MN%20%2055343,%20United%20States&auid=5864865717952427338&ll=44.924487,-93.420674&lsp=9902&q=Cream%20%26%20Amber)
* __[Bryant Lake Bowl](https://www.bryantlakebowl.com)__, vintage bowling alley (check out the [drone video](https://www.bryantlakebowl.com/drone-video)).
  * *Cost*: $$
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=810%20W%20Lake%20St,%20Minneapolis,%20MN%20%2055408,%20United%20States&auid=1576808993632497200&ll=44.948515,-93.290088&lsp=9902&q=Bryant%20Lake%20Bowl%20%26%20Theater)
* __[First Avenue](https://first-avenue.com)__, landmark music venue & side bar.
  * *Cost*: $
  * *Time commitment*: 1 hour
  * *Location*: [Map](https://maps.apple.com/?address=701%20N%20First%20Ave,%20Minneapolis,%20MN%20%2055403,%20United%20States&auid=17813292235307537780&ll=44.978286,-93.276029&lsp=9902&q=First%20Avenue%20%26%207th%20St%20Entry)
* __[Paisley Park](https://www.paisleypark.com)__, Prince's home and studio (tickets required).
  * *Cost*: $$$
  * *Time commitment*: 3 hours
  * *Location*: [Map](https://maps.apple.com/?address=7801%20Audubon%20Rd,%20Chanhassen,%20MN%2055317,%20United%20States&auid=16871254122203918531&ll=44.861828,-93.560548&lsp=9902&q=Paisley%20Park)

### For outdoor enthusiasts

* __[Lake Minnetonka](https://daytripper28.com/lake-minnetonka/)__, popular [large lake](https://en.wikipedia.org/wiki/Lake_Minnetonka) with 23 bays and 14 shoreline communities.
  * *Cost*: Free
  * *Time commitment*: 3 hours
  * *Location*: [Map](https://maps.apple.com/?address=Excelsior,%20MN,%20United%20States&auid=17913603809597655459&ll=44.902723,-93.568365&lsp=7618&q=Excelsior)
* __[Minnehaha Falls](https://www.minneapolisparks.org/parks-destinations/parks-lakes/minnehaha_regional_park/)__, majestic 53 foot waterfall.
  * *Cost*: Free
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=4801%20S%20Minnehaha%20Drive,%20Minneapolis,%20MN%2055417,%20United%20States&auid=13711197465404581256&ll=44.915359,-93.210808&lsp=9902&q=Minnehaha%20Falls)
* __[Minneapolis Chain of Lakes](https://www.minneapolisparks.org/parks-destinations/parks-lakes/minneapolis_chain_of_lakes_regional_park/)__, five lakes connected by trails.
  * *Cost*: Free
  * *Time commitment*: 3 hours
  * *Location*: [Map](https://maps.apple.com/?address=Bde%20Maka%20Ska,%20Minneapolis,%20MN,%20United%20States&auid=4067821214303608094&ll=44.941700,-93.308300&lsp=9902&q=Chain%20of%20Lakes)
* __[Minnesota Landscape Arboretum](https://arb.umn.edu)__, 1200 acre public demonstration garden (tickets required).
  * *Cost*: $$
  * *Time commitment*: 2 hours
  * *Location*: [Map](https://maps.apple.com/?address=3675%20Arboretum%20Drive,%20Chaska,%20MN%2055318,%20United%20States&auid=4752522237874615372&ll=44.862189,-93.616401&lsp=9902&q=Landscape%20Arboretum)
