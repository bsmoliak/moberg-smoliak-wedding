---
layout: page
title: Venue
description: More about the venue, Westwood Hills Nature Center.
image: pic-venue.jpg
---

## Introduction

Westwood Hills Nature Center is a 160-acre natural area featuring marsh,
woods, and restored prairie. It is located west of Minneapolis in the
City of St. Louis Park, about 4 miles from our home in Hopkins.

<h5>HGA Architects designed the new interpretive center</h5>
<div class="box alt">
		<div class="row uniform 50%">
			<iframe title="HGA Westwood Hills" src="https://player.vimeo.com/video/727117500?h=2f75448d45&amp;dnt=1&amp;app_id=122963" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture"></iframe>
		</div>
</div>

## History

In 1890 local businessman Thomas Barlow Walker and his colleagues
began purchasing land in the area of St. Louis Park for a business
development intended to attract new manufacturing industry to Minneapolis.
The land in the area of St. Louis Park at that time was being used primarily
for farming and dairy production. Nearby this early manufacturing
development, Minnesota’s first 27-hole golf course was developed and opened
for play in 1928.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="7u"><span class="image fit"><img src="assets/images/pic-venue-1937.png" alt="" /></span></div>
		</div>
</div>

In the mid-1950s when the golf course was no longer profitable, the
owners began to develop the land, selling off lots, and building houses
in the area. The City of St. Louis Park acquired land from Westwood Hills
Golf Course in 1958.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="7u"><span class="image fit"><img src="assets/images/pic-venue-1971.png" alt="" /></span></div>
		</div>
</div>


In 1959 during construction of some houses, a section of the drain tile broke that had
previously kept the Westwood Lake basin drained. The lake began to fill over the
following year. In 1971, the city began to explore the development of a nature center.
A decade later, in 1981, the Westwood Hills Nature Center’s first Interpretive Center
Building was opened, set back in the woods.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="7u"><span class="image fit"><img src="assets/images/pic-venue-2018.png" alt="" /></span></div>
		</div>
</div>

A new Interpretive Center opened in 2020 sited closer to the parking lot.
We discovered it on one of our weekly hikes during the pandemic lockdown.
The 13,500-square-foot building’s major sustainable features include a
closed loop geothermal system for heating and cooling and a 122-kilowatt
rooftop solar array to power the building. Over the course of the year,
solar panels offset electricity used. These design features inspired us and
when it came time to find a wedding venue, the choice was clear.

Westwood Hills Nature Center is managed according to a <a href="https://www.stlouisparkmn.gov/home/showpublisheddocument/12986/636840260016270000">Nature Resource
Management Plan</a> prepared in 2017. The following sections are excerpts
edited for brevity.

## Geology

The Twin Cities terrain is shaped primarily by glaciers, both directly and
indirectly. The area’s hills are largely glacial moraines — piles of sediment
hundreds of feet high, bulldozed by advancing glaciers in the last ice age
roughly 10,000 years ago. The historic bedrock surface influenced the way
in which the glaciers and their associated meltwater streams shaped the
landforms as they either planed or cut the bedrock respectively. These
glaciers gave the region its current topographic character.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="7u"><span class="image fit"><img src="assets/images/pic-receding-glacier.png" alt="" /></span></div>
		</div>
</div>

In the Twin Cities area, the Des Moines lobe, with drift generated from shale
and limestone, advanced from the west and over-rode the drift contributed
from the Superior Lobe that had advanced from the north where its sediments
were derived from the Lake Superior region. The meltwater streams of the
retreating glaciers deposited sand and gravel. The result were layers of
different tills and a topography defined by both glaciers. The lakes
and bogs of Hennepin County occur in depressions left by the melting of ice buried
under sediments or were created by the collapse and settling of glacial outwash.
Depth to bedrock varies in the vicinity of Westwood Hills Nature Center from
50-200 feet. At 114 feet above Westwood Lake, the terminal moraine on the
eastern edge of the property is one of the highest points in St. Louis Park.
This marks the maximum advance of the last glacier in the area.

## Soils

The geologic origin of the soils on the slopes of Westwood Hills Nature Center
are outwash deposits of sand, silty sand and gravel, overlain by a thin veneer
of clay loam or silt. The outwash was laid down by meltwater streams
originating from western Hennepin County and southern Anoka County.
High grade sand and gravel deposits occur in several locations in St. Louis
Park and several excavation sites were developed in the area. At the end of
World War II there were eight gravel mines in operation in the city of St.
Louis Park, some of which were still in operation into the 1960’s before
the resource was depleted and the land given over for other forms of
development. The gravel excavated from the local pits was used for
construction and road work in Minneapolis and the surrounding area.

## Vegetation

Westwood Hill Nature Center is a mix of open water, marsh, oak
woodland, restored prairie, and oak savannah.

### Oak Forest

The canopy and subcanopy of the oak forest is dominated by red
oak, American basswood, hackberry, sugar maple, black cherry,
green ash, and red elm. There is modest cover of several native
shrub species including wild currants, brambles, prickly ash,
and elderberry.

On the Northshore of Westwood Lake, mature cottonwoods are the
dominant trees along the shoreline, forming a canopy over
hackberry, boxelder, and silver maple. Islands of volunteer
trees including boxelder, black willow, green ash, black walnut,
and Russian olive have established in the altered habitat upland
from the shoreline.

### Shrub Carr

Along the southwestern shore, a shrub carr exists. A shrub carr
is a wetland community dominated by tall shrubs with a ground
flora dominated by grasses, sedges, and forbs typical of wet
meadows. Common native shrub species that occur in this unit
include elderberry, pussy willow, and red-twigged dogwood.
Green ash trees are common scattered throughout the shrub
carr. Red maple and silver maple have been planted along
the boardwalk.

### Pine Plantings

There is an altered woodland dominated in part by planted
evergreen species with additional cover provided by native
deciduous hardwood tree species. At least some of the evergreens
were planted as long ago as 1960. However, the conifer planting
has not been managed to attain a density that would support the
healthy growth of trees. Many of the trees in the unit are crowded,
with small diameters (dbh’s) and similarly stunted crowns.

### Restored Prairie

Prairie Restorations, Inc. began the planning and
implementation of the Westwood Hills prairie in 1978. Now
45 years old, the prairie is on an annual management plan. Prescribed
burns are conducted periodically and invasive species are being managed
with spot herbicide treatments. In 2009, the prairie was enhanced with
the addition of wildflowers near the edge along the driveway. The
prairie flora includes the native grasses big bluestem, indiangrass,
switchgrass, sideoats grama, and little bluestem. Native forbs include
hoary vervain, gray-headed coneflower, dotted blazingstar, wild
lavender bergamot, common ox-eye, compass plant, and cupplant.

### Westwood Lake

Westwood Lake is a DNR public waters. The open water or littoral
zone of Westwood Lake is 42 acres in area with a maximum lake
depth of 5 feet. Westwood Lake is in the Basset Creek Watershed.
The watershed of the landlocked lake is 463 acres.

## Wildlife

Westwood Hills Nature Center provides habitat for many animals,
including deer, turkeys, turtles, fox, mink, owls, and other birds.
At the interpretive center, you can view live animals, among them a
flying squirrel, a chipmunk, a red-tailed hawk, an American kestrel
and a barred owl.
