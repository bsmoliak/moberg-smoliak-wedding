---
layout: page
title: Getting There
description: Directions to the venue.
image: pic-getting-there.jpg
---

<h4>Y'all are traveling from near and far!</h4>
<div class="box alt">
		<div class="row uniform 50%">
			<div class="12u"><span class="image fit"><img src="assets/images/pic-guest-map.png" alt="" /></span></div>
		</div>
</div>

## Venue Address

<a href="http://maps.apple.com/?daddr=8300+W+Franklin+Ave,+St+Louis+Park+MN+55426">
8300 W Franklin Ave, St Louis Park, MN 55426</a>

## By plane

Domestic and International flights to the wedding should arrive at
the Minneapolis-St. Paul International Airport (MSP). The airport
is connected to Downtown Minneapolis via the Metro Transit Blue Line
Light Rail. App-based rideshare services such as Uber and Lyft are also
available at the airport.

## By rideshare

The venue is served by Uber and Lyft app-based rideshare. **For out-of-town
guests staying overnight, we will provide a complementary Lyft event code
to get back to their accommodations safely.**

## By car

__From the North__, exit US-169 to I-394 east towards Minneapolis.
Follow signs for General Mills Blvd, turn left on General Mills Blvd.
Turn left onto the S Frontage Rd/Wayzata Blvd. Turn right onto Texas
Ave S. Turn right onto W Franklin Ave. The Nature Center entrance
will be on the right.

__From the East__, navigate to I-394 west towards Minnetonka. Take
exit 4 for Louisiana Ave. Turn left on Louisiana Ave. Turn right at
the 2nd cross street onto S Frontage Rd/Wayzata Blvd. Turn left onto
Texas Ave S. Turn right onto W Franklin Ave. The Nature Center entrance
will be on the right.

__From the West__, navigate to I-394 east towards Minneapolis. Follow
signs for General Mills Blvd, turn left on General Mills Blvd.
Turn left onto the S Frontage Rd/Wayzata Blvd. Turn right onto Texas
Ave S. Turn right onto W Franklin Ave. The Nature Center entrance
will be on the right.

__From the South__, exit US-169 at Cedar Lake Rd. Turn left onto Jordan
Ave S. At Cedar Lake Road, turn right. Turn left at Texas Ave. Turn left
onto W Franklin Ave. The Nature Center entrance will be on the right.

**Car Parking** is available in a lot at the venue. The gate is locked at
midnight, so any cars left behind cannot be picked up until the gate
opens at 10am on Sunday. To avoid this situation, street parking
is also available outside the venue along W Franklin Ave.

## By bike

Westwood Hills Nature Center can be accessed by bike via the North
Cedar Lake Trail.

__From Minneapolis__, ride west to St. Louis Park.
At the Virigina Ave S street crossing, turn right. At Cedar Lake
Road, turn right. At Texas Ave S, turn left. At Franklin Ave W,
turn left. The Nature Center entrance will be on the right.

__From Hopkins__, ride east to St. Louis Park. At the Virginia
Ave S street cross, turn left. At Cedar Lake Road, turn right.
At Texas Ave S, turn left. At Franklin Ave W, turn left. The Nature
Center entrance will be on the right.

**Bike Parking** is available outside the east entrance to the
interpretive center.
