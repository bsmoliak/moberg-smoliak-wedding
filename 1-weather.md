---
layout: page
title: Weather
description: Brian's official weather forecast.
image: pic-weather.jpg
---

## Official Forecast

*Scroll down for a weather briefing and stay tuned for
updates throughout the remainder of the week*

### Saturday

* Rain likely overnight, clearing by 8am
* Cloudy
* High temperature: 72°F
* Chance of rain showers afternoon, rain likely in evening

### Sunday

* Rain overnight, clearing by 8am
* Skies clearing from overcast to partly cloudy
* High temperature: 70°F
* Chance of rain diminishing throughout the day

##  Saturday Weather Briefing - Posted September 23rd

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/ba15b49673a2456eb10b19c63db8caa7?sid=9fffbf91-3c49-4763-bd3a-d6b58f384574" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

<!-- ## Tuesday Weather Briefing - Posted September 19th

<div style="position: relative; padding-bottom: 56.42633228840126%; height: 0;"><iframe src="https://www.loom.com/embed/09e4fdaee9cb47109500cff162846451?sid=9c45ad7d-a18e-4eee-b6fd-04fd279b5420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

-->

<br><br>

## 6-10 Day Outlook - Posted September 15th

<div class="box alt">
		<div class="row uniform 50%">
			<div class="5u"><p>NOAA's Climate Prediction Center 6-10 Day Outlook highlights an even higher likelihood of above normal temperatures and precipitation during the week of the wedding (compared to the 8-14 Day Outlook below).</p>
			<p>What does this mean for the wedding weekend? Expect afternoon maximum
			temperatures in the 70s, humidity bordering on muggy,
			and the potential for nusiance rain showers. Some say rain on your wedding day
			is good luck. Check the forecast leading up to the wedding to find out
			just how much luck we have.</p>
			</div>
			<div class="3u"><span class="image fit"><img src="assets/images/pic-6-10-outlook.png" alt="" /></span></div>
		</div>
</div>

<div class="box alt">
		<div class="row uniform 50%">
			<div class="5u"><span class="image fit"><img src="assets/images/pic-6-10-temperature.png" alt="" /></span></div>
			<div class="5u"><span class="image fit"><img src="assets/images/pic-6-10-precipitation.png" alt="" /></span></div>
		</div>
</div>

## 8-14 Day Outlook - Posted September 12th


<div class="box alt">
		<div class="row uniform 50%">
			<div class="5u"><p>NOAA's Climate Prediction Center 8-14 Day Outlook highlights a likelihood of above normal temperatures and precipitation during the week of the wedding.</p><p>This suggests high temperatures in the 70s or 80s. It also implies that the atmospheric circulation pattern will be capable of rainfall. That said, it is too early to say whether it will rain during the wedding.</p>
			</div>
			<div class="3u"><span class="image fit"><img src="assets/images/pic-8-14-outlook.png" alt="" /></span></div>
		</div>
</div>


<div class="box alt">
		<div class="row uniform 50%">
			<div class="5u"><span class="image fit"><img src="assets/images/pic-8-14-temperature.png" alt="" /></span></div>
			<div class="5u"><span class="image fit"><img src="assets/images/pic-8-14-precipitation.png" alt="" /></span></div>
		</div>
</div>

## Climatology - Posted September 10th

On average over the last 30 years, September 23rd is the epitome
of an autumn day in the Twin Cities.

### Daylight

This year's fall equinox occurs early in the morning of September 23rd.
Sunrise is at 7:01am and sunset is about 12 hours later at 7:08pm.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="10u"><span class="image fit"><img src="assets/images/pic-average-daylight.png" alt="" /></span></div>
		</div>
</div>

### Temperature

Temperatures typically vary from the lower 50s in the morning
to the upper 60s in the afternoon. The maximum temperature
normally occurs around 4:00pm, after which cooling occurs
through the evening.

Year to year variability is large in autumn in Minnesota. The
high temperature could be as low as 50°F or as high as 85°F. In
other words, check updated forecasts closer to the wedding day
for more information.


<div class="box alt">
		<div class="row uniform 50%">
			<div class="10u"><span class="image fit"><img src="assets/images/pic-average-temperature.png" alt="" /></span></div>
		</div>
</div>

### Precipitation

Rain falls on September 23rd in 30% of years, or 3 out of 10 years. It rained
on September 23rd last year. Will this year be dry?

### Clouds

Given the average chance of precipitation, it should be unsurprising to learn
that about 30% of years see overcast skies in Minneapolis on September
23rd. Conversely, 40% of years see clear skies. In the remaining 30% of years
partly cloudy skies prevail.

<div class="box alt">
		<div class="row uniform 50%">
			<div class="10u"><span class="image fit"><img src="assets/images/pic-average-clouds.png" alt="" /></span></div>
		</div>
</div>
