# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.3.1]

### Changed

* Fix weather briefing issue


## [v1.3.0]

### Added

* Final weather briefing


## [v1.2.1]

### Added

* Button to weather update on main page


## [v1.2.0]

### Added

* Official forecast
* Weather briefing

### Changed

* Fall color update


## [v1.1.0]

### Added

* Guest map
* 6-10 day outlook

### Changed

* Menu detail


## [v1.0.3]

### Changed

* Fixed even more typos


## [v1.0.2]

### Changed

* Fixed more typos


## [v1.0.1]

### Changed

* Fixed typos


## [v1.0.0]

### Changed

* Release for public consumption

### Removed

* Elements page hidden


## [v0.6.0]

### Added

* Pages
  * Guide

### Changed

* Fixed typos


## [v0.5.0]

### Added

* Pages
  * Venue


## [v0.4.0]

### Added

* Pages
  * Program
  * Guide


## [v0.3.0]

### Added

* Pages
  * Weather

### Changed

* Configuration
  * Removed www from URL


## [v0.2.0]

### Added

* Pages
  * Getting There

### Changed

* Spacing of section one


## [v0.1.1]

### Changed

* Image path specification


## [v0.1.0]

### Added

* Initial structure


[v1.3.1]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.3.0...v1.3.1
[v1.3.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.2.1...v1.3.0
[v1.2.1]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.2.0...v1.2.1
[v1.2.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.0.3...v1.1.0
[v1.0.3]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.0.1...v1.0.2
[v1.0.1]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.6.0...v1.0.0
[v0.6.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.5.0...v0.6.0
[v0.5.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.4.0...v0.5.0
[v0.4.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.3.0...v0.4.0
[v0.3.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.1.1...v0.2.0
[v0.1.1]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/compare/v0.1.0...v0.1.1
[v0.1.0]: https://gitlab.com/bsmoliak/moberg-smoliak-wedding/-/tree/v0.1.0
